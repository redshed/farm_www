#!/usr/bin/env bash
echo "pull from repo"
sudo git pull origin development
echo "update dependencies"
sudo npm install
echo "restart server"
sudo pm2 kill
sudo pm2 start farm_www.yaml