'use strict';

var express = require('express');
var config = require('config');
var environments = require("./config/environments.js").environments; //not sure there is a better way to basically get an
var package_json = require('./package.json');
var path = require('path');
var hbs = require('hbs');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var util = require('./utility');

//logging
var expressWinston = require('express-winston');
var detailLogger = require('./detail_logger.js');
var contextService = require('request-context');

//app
var main = require('./routes/main');
var user_data = require('./routes/user-data');
var auth = require('./routes/auth');
var pfdata = require('./routes/pfdata');

var app = express();

//environment and logging
var logger = detailLogger.logger;

var env = config.get("label");
app.logger = logger;

//get current environment
logger.verbose('app env / NODE_ENV = ' + (app.get('env') || 'not set'));
logger.verbose('config label = ' + (env || 'not set'));
logger.verbose('version ' + package_json.version);

//express middleware logging
app.use(expressWinston.logger({
    transports: detailLogger.winston_transports,
    meta: true, // optional: control whether you want to log the meta data about the request (default to true)
    msg: "HTTP {{req.method}} {{req.url}}", // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
    expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
    colorize: true, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
    ignoreRoute: function ignoreRoute(req, res) {
        return false;
    } // optional: allows to skip some log messages based on request and/or response
}));

app.use(expressWinston.errorLogger({
    transports: detailLogger.winston_transports
}));
//END LOGGING

//PASSPORT SETUP
app.use(require('express-session')({ secret: 'Red Shed', resave: false, saveUninitialized: false }));
//AUTH / PASSPORT SETUP
var passport = require('passport');
var Auth0Strategy = require('passport-auth0');

// Configure Passport to use Auth0
var strategy = new Auth0Strategy({
    domain: config.get("auth0_domain"),
    clientID: config.get("auth0_client_id"),
    clientSecret: config.get("auth0_client_secret"),
    callbackURL: config.get("auth0_callback_url")
}, function (accessToken, refreshToken, extraParams, profile, done) {
    return done(null, profile);
});

passport.use(strategy);

// This can be used to keep a smaller payload
passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (user, done) {
    done(null, user);
});

app.use(passport.initialize());
app.use(passport.session());

// view engine setup
hbs.registerPartials(__dirname + '/views/partials');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(bodyParser.json({ limit: '50mb', parameterLimit: 10000 }));
app.use(bodyParser.urlencoded({ limit: '50mb', parameterLimit: 10000, extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var redirect = require('express-simple-redirect');
var index = require('./routes/index');
var user = require('./routes/user');

app.use('/', index);
app.use('/user', user);
app.use('/pfdata', pfdata);
app.use('/user_data', user_data);
app.use('/main', main);

// app.use(redirect({
//     '/': '/main/farm'
// }));

//catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

//error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
//# sourceMappingURL=app.js.map