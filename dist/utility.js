'use strict';

// const authorized = function(req, res, next)  {
//     if(req.user) {
//         return next();
//     }
//     return res.redirect('/login');
// };

var Hashids = require('hashids');
var hashids = new Hashids('Red Shed');

var hash_encode = function hash_encode(s) {
    var hex = new Buffer(s).toString('hex');
    var encoded = hashids.encodeHex(hex);
    return encoded;
};

var hash_decode = function hash_decode(h) {
    var decodedHex = hashids.decodeHex(h);
    var s = new Buffer(h, 'hex').toString('utf8');
    return s;
};

var ua = require('universal-analytics');
var init_ua_visitor = function init_ua_visitor(ga_id, user_id) {
    var visitor = ua(ga_id, user_id, { strictCidFormat: false });
    return visitor;
};

module.exports = { hash_encode: hash_encode, hash_decode: hash_decode, init_ua_visitor: init_ua_visitor };
//# sourceMappingURL=utility.js.map