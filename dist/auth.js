'use strict';

var authorized = function authorized(req, res, next) {
    if (req.session.user && req.cookies.user_sid) {
        res.redirect('/dashboard');
    } else {
        next();
    }
};
//# sourceMappingURL=auth.js.map