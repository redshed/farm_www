"use strict";

farmTitleService = {
    //all these use the red shed test user
    key: null,
    full_key: null,

    authenticateUser: function authenticateUser() {
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "https://test.propertyforcemobile.com/v4test/TitleService.svc/Secure/AuthenticateUser?username=redshedt&office=redshed&password=U2hlZHJlZDQh&devicedata=APPNAME%7CPropertyForce%2CAPPVERSION%7C4.0.0.0-DEV%2CUSERNAME%7Credshedp%2COFFICECODE%7Credshed%2CDEVICETYPE%7Csamsung%20SM-G920V%2CDEVICEDIMENSIONS%7CUndetermined%2COS%7CAndroid%2COSVERSION%7C5.0.2",
            "method": "GET",
            "headers": {
                "cache-control": "no-cache",
                "postman-token": "923e2fba-0523-279e-6f65-0e16e173afd2"
            }
        };

        $.ajax(settings).done(function (response) {
            console.log(response);
        });
    },

    getRepDataWithKey: function getRepDataWithKey() {
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "https://test.propertyforcemobile.com/v4test/TitleService.svc/Secure/GetRepDataWithKey?username=redshedt&office=redshed&password=U2hlZHJlZDQh&devicedata=APPNAME%7CPropertyForce%2CAPPVERSION%7C4.0.0.0-DEV%2CUSERNAME%7Credshedp%2COFFICECODE%7Credshed%2CDEVICETYPE%7Csamsung%20SM-G920V%2CDEVICEDIMENSIONS%7CUndetermined%2COS%7CAndroid%2COSVERSION%7C5.0.2",
            "method": "GET",
            "headers": {
                "cache-control": "no-cache",
                "postman-token": "a90ee245-ebe7-3510-8619-ea1a20a32a9d"
            }
        };

        $.ajax(settings).done(function (response) {
            console.log(response);
            farmTitleService.full_key = response.json().d.Key;
            farmTitleService.key = full_key.split("_")[0];
            console.log(farmTitleService.full_key);
            console.log(farmTitleService.key);
        });
    },

    getFarmReportV2: function getFarmReportV2(shape_json) {
        //get the key if needed
        if (farmTitleService.key === null) {
            farmTitleService.authenticateUser();
            farmTitleService.getRepDataWithKey();
        }

        shape_json = { "points": [{ "x": 44.37578480863809, "y": -103.7305842541931 }, { "x": 44.3727980275858, "y": -103.7304749557464 }, { "x": 44.37258768515032, "y": -103.7268008464245 }, { "x": 44.3756526002243, "y": -103.7266495101137 }, { "x": 44.37578480863809, "y": -103.7305842541931 }] };
        //construct url
        var url = "https://test.propertyforcemobile.com/v4test/TitleService.svc/Secure/GetFarmReportv2?key=";
        url += farmTitleService.key;
        url += "&devicedata=APPNAME%7CPropertyForce%2CAPPVERSION%7C4.0.0.0-DEV%2CUSERNAME%7Credshedp%2COFFICECODE%7Credshed%2CDEVICETYPE%7Csamsung%20SM-G920V%2CDEVICEDIMENSIONS%7CUndetermined%2COS%7CAndroid%2COSVERSION%7C5.0.2&filterJson=%7B%22MaxBathrooms%22%3Anull%2C%22UseCodes%22%3Anull%2C%22MaxBedrooms%22%3Anull%2C%22StartDate%22%3Anull%2C%22MinBathrooms%22%3Anull%2C%22EndDate%22%3Anull%2C%22SaleTypes%22%3Anull%2C%22OwnerOccupied%22%3Anull%2C%22MinBedrooms%22%3Anull%7D&shapeJson=";
        url += JSON.stringify(shape_json);
        //url encode?
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": url,
            "method": "GET",
            "headers": {
                "cache-control": "no-cache",
                "postman-token": "197bf4c7-3ab9-baad-5273-02d7c5f84fca"
            }
        };

        $.ajax(settings).done(function (response) {
            console.log(response);
            return response;
        });
    }
};
//# sourceMappingURL=farmTitleService.js.map
//# sourceMappingURL=farmTitleService.js.map