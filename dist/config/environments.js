'use strict';

//these match the label in the environment json file
//set through NODE_ENV environment variable because of convention
//and some of the internals of express seem to expect this to have a value of "production" in production
//
var environments = {
    autotest: 'autotest',
    development: 'development',
    production: 'production',
    qa: 'qa'
};

module.exports = {
    environments: environments
};
//# sourceMappingURL=environments.js.map