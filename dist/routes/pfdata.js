'use strict';

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var express = require('express');
var router = express.Router();
var config = require('config');
var request = require('superagent');
var querystring = require('querystring');

//const authorized = require('../utility').authorized;
var ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
var detailLogger = require('../detail_logger.js');
var logger = detailLogger.logger;

//farm search
router.post('/farm', ensureLoggedIn, function (req, res, next) {
    //better to do this as a pass-through to hide auth
    res.setHeader('Content-Type', 'application/json');
    var token = config.get("pfdata_access_token");
    var url = config.get("pfdata_url") + "/api/farm?access_token=" + token + "&" + querystring.stringify(req.query);
    logger.verbose("request to: " + url.replace(token, "TOKEN_MASK"));
    request.post(url).send(req.body).set('accept', 'json').end(function (err, api_res) {
        // Calling the end function will send the request
        logger.debug("response:" + (0, _stringify2.default)(api_res.body));
        res.end((0, _stringify2.default)(api_res.body));
    });
});

module.exports = router;
//# sourceMappingURL=pfdata.js.map