'use strict';

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var express = require('express');
var router = express.Router();
var config = require('config');
//const authorized = require('../utility').authorized;
var ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
var logger = require('../detail_logger.js').logger;
var ParseFarms = require('../parse_farms');

//get / list farms
router.get('/farm', ensureLoggedIn, function (req, res, next) {
    logger.verbose("calling parse server to list farms");
    res.setHeader('Content-Type', 'application/json');
    var app_name = config.get("parse_server_appname");
    var parse_farms = new ParseFarms.ParseFarms();
    var email = req.user.email || req.session.passport.user.displayName;
    parse_farms.loadFarms(app_name, email).then(function (r) {
        //return json list of farms
        if (r) {
            logger.verbose("/user-data/farm found " + r.length + " farms");
            res.end((0, _stringify2.default)(r));
        } else {
            res.end("");
        }
    }).catch(function (e) {
        logger.error(e);
        res.end("");
    });
});

router.put('/farm/:objectId', ensureLoggedIn, function (req, res, next) {
    //save to parse server
    logger.verbose("calling parse server to update farm");
    res.setHeader('Content-Type', 'application/json');

    var objectId = req.params.objectId;
    var properties = req.body.properties;
    var parse_farms = new ParseFarms.ParseFarms();

    //TODO: for now only update the properties array, maybe other edits later (like name)
    parse_farms.updateFarmProperties(objectId, properties).then(function (r) {
        //return json list of farms
        if (r) {
            logger.verbose("/user-data/farm updated " + objectId);
            res.end((0, _stringify2.default)(r));
        } else {
            res.end("");
        }
    }).catch(function (e) {
        logger.error(e);
        res.end("");
    });
});

//save new farm
router.post('/farm', ensureLoggedIn, function (req, res, next) {
    //save to parse server
    logger.verbose("calling parse server to save farm");
    res.setHeader('Content-Type', 'application/json');
    var app_name = config.get("parse_server_appname");
    var parse_farms = new ParseFarms.ParseFarms();
    var new_farm = ParseFarms.FarmJson;
    var email = req.user.email || req.session.passport.user.displayName;
    new_farm.appName = app_name;
    new_farm.properties = req.body.Properties;
    new_farm.propertyCount = parseInt(req.body.Count) || 0;
    new_farm.turnoverRate = req.body.TurnoverPercent;
    new_farm.shapeCoordinates = (0, _stringify2.default)(req.body.shapeCoordinates);
    new_farm.reportDate = new Date();
    new_farm.webServiceId = req.body.Token;
    new_farm.email = email;
    new_farm.name = req.body.name;
    new_farm.nameLower = new_farm.name.toLowerCase();

    parse_farms.saveFarm(app_name, new_farm).then(function (r) {
        //return json list of farms
        if (r) {
            logger.verbose("/user-data/farm saved " + new_farm.objectId);
            res.end((0, _stringify2.default)(r));
        } else {
            res.end("");
        }
    }).catch(function (e) {
        logger.error(e);
        res.end("");
    });
});

module.exports = router;
//# sourceMappingURL=user-data.js.map