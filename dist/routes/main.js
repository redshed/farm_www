'use strict';

var express = require('express');
var router = express.Router();
var package_json = require('../package.json');
//const authorized = require('../utility').authorized;
var ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();

router.get('/', ensureLoggedIn, function (req, res, next) {
    res.redirect('/farm');
});

router.get('/farm', ensureLoggedIn, function (req, res, next) {
    res.render('farm', { title: 'PropertyForce New Farm', version: package_json.version });
});

router.get('/savedfarms', ensureLoggedIn, function (req, res, next) {
    res.render('savedfarms', { title: 'PropertyForce Saved Farms', version: package_json.version });
});

router.get('/savedfarmdetail/:objectId', ensureLoggedIn, function (req, res, next) {
    res.render('savedfarmdetail', { title: 'PropertyForce Saved Farm', version: package_json.version, objectId: req.param("objectId") });
});

module.exports = router;
//# sourceMappingURL=main.js.map