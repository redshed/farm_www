'use strict';

var express = require('express');
var package_json = require('../package.json');
var Parse = require('parse/node');
var router = express.Router();
var config = require('config');

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    return res.redirect('/login');
}

//get / list farms
router.get('/', isLoggedIn, function (req, res, next) {
    res.render('farm', { title: 'PropertyForce Farm Test', version: package_json.version, pfdata_url: config.get("pfdata_url") });
});

//save farm
router.post('/testsave', function (req, res, next) {
    Parse.initialize("XYGx5GJAFUvkhLdcsqd7QgyLYRQ4y1v5");
    Parse.serverURL = 'https://parse-server-qa.herokuapp.com/pforce-consolidated-test/parse/';
    var Farm = Parse.Object.extend("Farm");
    var farm = new Farm();
    farm.save({
        "email": "sample_farm@redshedtech.com",
        "reportVersion": 1,
        "reportDate": {
            "__type": "Date",
            "iso": "2017-12-13T22:28:56.618Z"
        },
        "isDeleted": false,
        "reportCaveats": [],
        "appName": "propertyforce",
        "indexOfLastPropertyViewed": 0,
        "turnoverRate": "10.7%",
        "nameLower": "farmsavetest2",
        "propertyCount": 28,
        "webServiceId": "20b8136f-8840-497b-8c06-a149b9fcf332",
        "name": "farmtest"
    }, {
        success: function success(farm) {
            res.send("OK");
        },
        error: function error(farm, _error) {
            res.send("error");
        }
    });
});

module.exports = router;
//# sourceMappingURL=user-data.js.map