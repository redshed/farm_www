'use strict';

var express = require('express');
var router = express.Router();
var detailLogger = require('../detail_logger.js');
var logger = detailLogger.logger;
var passport = require('passport');

// app.get('/login',
//     passport.authenticate('auth0', {}), function (req, res) {
//         res.redirect("/");
//     });

router.route('/login', passport.authenticate('auth0', {}), function (req, res) {
    res.redirect("/");
});
// .post(function(req, res, next) {
//     passport.authenticate('titleservice-auth', function(err, user, info) {
//         if (err) {
//             return next(err); // will generate a 500 error
//         }
//         if (!user) {
//             return res.status(409).render('login', {errMsg: info.errMsg});
//         }
//         req.login(user, function(err) {
//             if (err) {
//                 logger.error(err);
//                 return next(err);
//             }
//             return res.redirect('/main/farm');
//         });
//     })(req, res, next);
// });

// router.route('/cognito')
//     .post( function (req, res , next) {
//             passport.authenticate('cognito', function(err, user, info) {
//                 if (err) { return next(err); }
//                 if (!user) { return res.redirect('/login'); }
//                 req.logIn(user, function(err) {
//                     if (err) { return next(err); }
//                     return res.redirect('/users/' + user.username);
//                 });
//             })(req, res, next);
//         });


router.route('/logout').get(function (req, res, next) {
    req.logout();
    req.session.destroy();
    return res.redirect('/auth/login');
});

module.exports = router;
//# sourceMappingURL=auth.js.map