/**
 * Created by dvicente@solidgear.es on 10/11/2017
 */
'use strict';

var winston = require('winston');
require('winston-papertrail').Papertrail;
var uuid = require('uuid');
var contextService = require('request-context');
var config = require('config');
var environments = require("./config/environments.js").environments; //not sure there is a better way to basically get an enum
var env = config.get("label");

//LOGGING
//set console for local development
//papertrail for the rest
var winston_transports = [new winston.transports.Papertrail({
    host: 'logs4.papertrailapp.com',
    port: 25947,
    program: 'farm_www',
    colorize: true,
    level: 'info'
})];

if (env === environments.development || env === environments.autotest) {
    winston_transports = [new winston.transports.Console({
        colorize: true,
        level: 'debug'
    })];
}

var winstonLogger = new winston.Logger({
    transports: winston_transports,
    exitOnError: false
});

winston.configure({
    transports: winston_transports,
    exceptionHandlers: winston_transports
});

winstonLogger.stream = {
    write: function write(message, encoding) {
        winstonLogger.info(message);
    }
};

// Wrap Winston logger to print reqId in each log
var formatMessage = function formatMessage(message) {
    var request_token = getToken();
    message = request_token ? request_token + " " + message : message;
    return message;
};

var logger = {
    log: function log(level, message) {
        winstonLogger.log(level, formatMessage(message));
    },
    error: function error(message) {
        winstonLogger.error(formatMessage(message));
    },
    warn: function warn(message) {
        winstonLogger.warn(formatMessage(message));
    },
    verbose: function verbose(message) {
        winstonLogger.verbose(formatMessage(message));
    },
    info: function info(message) {
        winstonLogger.info(formatMessage(message));
    },
    debug: function debug(message) {
        winstonLogger.debug(formatMessage(message));
    },
    silly: function silly(message) {
        winstonLogger.silly(formatMessage(message));
    }
};

var setToken = function setToken(tag) {
    contextService.set("request:token", tag + "-" + uuid.v4().replace(/-/g, "").substring(0, 16));
};

var getToken = function getToken() {
    return contextService.get("request:token");
};

module.exports = { logger: logger, winston_transports: winston_transports, setToken: setToken, getToken: getToken };
//# sourceMappingURL=detail_logger.js.map