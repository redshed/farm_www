"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var request = require("request-promise");
var _ = require("underscore");
var btoa = require("btoa");
var logger = require('./detail_logger').logger;

var RedShedTitleService = function () {
    function RedShedTitleService() {
        _classCallCheck(this, RedShedTitleService);

        this.default_options = {
            method: 'GET',
            url: '',
            qs: {
                username: '',
                office: '',
                password: '',
                devicedata: ''
            },
            headers: {
                'cache-control': 'no-cache'
            }
        };
    }

    _createClass(RedShedTitleService, [{
        key: "authenticateUser",
        value: function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(username, office, password) {
                var device_data, options, status, message, key, user_response, key_response, full_key;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                device_data = "APPNAME|PropertyForce,APPVERSION|4.0.0.0-DEV,USERNAME|" + username + ",OFFICECODE|" + office + ",DEVICETYPE|samsung SM-G920V,DEVICEDIMENSIONS|Undetermined,OS|Android,OSVERSION|5.0.2";
                                options = _.clone(this.default_options);
                                status = "FAIL";
                                message = "";
                                key = "";

                                options.qs.username = username;
                                options.qs.office = office;
                                options.qs.password = btoa(password);
                                options.qs.devicedata = device_data;
                                options.url = 'https://test.propertyforcemobile.com/v4test2/TitleService.svc/Secure/AuthenticateUser';
                                _context.next = 12;
                                return request(options);

                            case 12:
                                user_response = _context.sent;

                                user_response = JSON.parse(user_response);

                                status = user_response.d.StatusCode;
                                message = user_response.d.Status;

                                if (!(status === "OK")) {
                                    _context.next = 23;
                                    break;
                                }

                                options.url = 'https://test.propertyforcemobile.com/v4test2/TitleService.svc/Secure/GetRepDataWithKey';
                                _context.next = 20;
                                return request(options);

                            case 20:
                                key_response = _context.sent;

                                key_response = JSON.parse(key_response);
                                try {
                                    if (key_response.d.Key && key_response.d.Key.length > 0) {
                                        full_key = JSON.parse(key_response).d.Key;

                                        key = full_key.split("_")[0];
                                    } else {
                                        status = key_response.d.Status;
                                        message = key_response.d.StatusCode;
                                    }
                                } catch (e) {
                                    logger.error("Error in titleservice authenticateUser");
                                }

                            case 23:
                                return _context.abrupt("return", { username: username, key: key, status: status, message: message });

                            case 24:
                            case "end":
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            function authenticateUser(_x, _x2, _x3) {
                return _ref.apply(this, arguments);
            }

            return authenticateUser;
        }()
    }]);

    return RedShedTitleService;
}();

module.exports = { RedShedTitleService: RedShedTitleService };
//# sourceMappingURL=titleservice.js.map