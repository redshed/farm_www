'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var logger = require('./detail_logger').logger;

var Parse = require('parse/node');
var config = require('config');

var parse_server_url = config.get("parse_server_url");
var parse_server_token = config.get("parse_server_token");

var ParseFarms = function () {
    function ParseFarms() {
        (0, _classCallCheck3.default)(this, ParseFarms);
    }

    (0, _createClass3.default)(ParseFarms, [{
        key: 'loadFarms',
        value: function () {
            var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(app_name, email) {
                var Farm, query, farms;
                return _regenerator2.default.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                logger.verbose("calling parse server to list farms");
                                Parse.initialize(parse_server_token);
                                Parse.serverURL = parse_server_url;
                                Farm = Parse.Object.extend("Farm");
                                query = new Parse.Query(Farm);

                                query.equalTo("email", email);
                                query.equalTo("isDeleted", false);
                                _context.next = 9;
                                return query.find();

                            case 9:
                                farms = _context.sent;

                                logger.verbose("list farms completed");
                                return _context.abrupt('return', farms.map(function (f) {
                                    return f.toJSON();
                                }));

                            case 12:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            function loadFarms(_x, _x2) {
                return _ref.apply(this, arguments);
            }

            return loadFarms;
        }()
    }, {
        key: 'saveFarm',
        value: function () {
            var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(app_name, new_farm) {
                var Farm, parse_farm;
                return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                logger.verbose("calling parse server to save farm");
                                Parse.initialize(parse_server_token);
                                Parse.serverURL = parse_server_url;
                                Farm = Parse.Object.extend("Farm");
                                parse_farm = new Farm();

                                parse_farm.save(new_farm, {
                                    success: function success(farm) {
                                        // Execute any logic that should take place after the object is saved.
                                        logger.verbose('New farm created with objectId: ' + farm.id);
                                        return new_farm;
                                    },
                                    error: function error(farm, _error) {
                                        // Execute any logic that should take place if the save fails.
                                        // error is a Parse.Error with an error code and message.
                                        logger.error('Failed to create new farm, with error code: ' + _error.message);
                                        return { error: _error };
                                    }
                                });

                            case 6:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, this);
            }));

            function saveFarm(_x3, _x4) {
                return _ref2.apply(this, arguments);
            }

            return saveFarm;
        }()
    }, {
        key: 'updateFarmProperties',
        value: function () {
            var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(objectId, properties) {
                var Farm, query;
                return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                logger.verbose("calling parse server to update farm");
                                Parse.initialize(parse_server_token);
                                Parse.serverURL = parse_server_url;
                                Farm = Parse.Object.extend("Farm");
                                query = new Parse.Query(Farm);

                                query.get(objectId, {
                                    success: function success(farm) {
                                        farm.set("properties", properties);
                                        farm.save(null, {
                                            success: function success(o) {
                                                console.log(o);
                                                return o;
                                            },
                                            error: function error(model, _error2) {
                                                console.log("parse_farms updateFarmProperties ERROR " + _error2);
                                                return { error: _error2 };
                                            }
                                        });
                                    },
                                    error: function error(object, _error3) {
                                        logger.error("failed to get farm for objectId " + objectId + " - " + _error3);
                                    }

                                });

                            case 6:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, this);
            }));

            function updateFarmProperties(_x5, _x6) {
                return _ref3.apply(this, arguments);
            }

            return updateFarmProperties;
        }()
    }]);
    return ParseFarms;
}();

var FarmJson = {
    "objectId": "",
    "filters": "{\"MaxBedrooms\":null,\"MaxBathrooms\":null,\"SaleTypes\":null,\"MinBedrooms\":null,\"UseCodes\":null,\"EndDate\":null,\"MinBathrooms\":null,\"OwnerOccupied\":null,\"StartDate\":null}",
    "shapeCoordinates": null,
    "properties": null,
    "reportDate": null,
    "reportCaveats": [],
    "email": "",
    "reportVersion": 1,
    "isDeleted": false,
    "appName": "",
    "indexOfLastPropertyViewed": 0,
    "turnoverRate": "",
    "nameLower": "",
    "propertyCount": 0,
    "webServiceId": "",
    "name": "",
    "isPurchased": false,
    "createdAt": null,
    "updatedAt": null
};

module.exports = { ParseFarms: ParseFarms, FarmJson: FarmJson };
//# sourceMappingURL=parse_farms.js.map