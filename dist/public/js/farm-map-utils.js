'use strict';

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//utilities for google maps
var currentOverlay = null;
var allOverlays = [];
var current_map = null;
var json_container = null;

farmMapUtils = {
    lastShapePoints: null,

    setupMapSimple: function setupMapSimple(map_id) {

        //MAP SETUP
        var mapOptions = {
            zoom: 16,
            center: { lat: 33.701990, lng: -117.86014 },
            mapTypeId: google.maps.MapTypeId.RoadMap
        };
        return new google.maps.Map(document.getElementById(map_id), mapOptions);
    },

    //bind the map
    //set up address autocomplete
    //set up polygon drawing
    //called by googleapis callback
    //{lat: -33, lng: 151}
    //(33.701990, -117.86014)
    setupMap: function setupMap(map_id, text_autocomplete_id, shape_points_json_container, reset_overlay_selector) {

        //MAP SETUP
        var mapOptions = {
            zoom: 16,
            center: { lat: 33.701990, lng: -117.86014 },
            mapTypeId: google.maps.MapTypeId.RoadMap
        };
        map = new google.maps.Map(document.getElementById(map_id), mapOptions);

        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            map.setCenter(pos);
        }, function (err) {
            console.log(err);
        });

        //ADDRESS AUTOCOMPLETE
        var input = document.getElementById(text_autocomplete_id);
        var options = {
            types: ['address']
        };

        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });

        var autocomplete = new google.maps.places.Autocomplete(input, options);

        autocomplete.addListener('place_changed', function () {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                //map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);
            //farmMapUtils.setupOverlay(map, shape_points_json_container);
        });

        var reset_button = function reset_button() {
            var btn = $('<button class="btn btn-sm btn-icon btn-primary" id="bt_reset_farm_drawing_polygon"><i class="ti-home"></i></button>');
            btn[0].addEventListener('click', function () {
                farmMapUtils.clearOverlays();
                farmMapUtils.setupOverlay();
            });
            return btn[0];
        };

        $('#bt_draw_farm').click(function () {
            farmMapUtils.clearOverlays();
            farmMapUtils.setupOverlay();
        });

        map.controls[google.maps.ControlPosition.TOP_CENTER].push(reset_button());
        $('#bt_reset_farm_drawing_polygon').css("background-color", "yellow");

        current_map = map;
        json_container = shape_points_json_container;

        farmMapUtils.setupOverlay();

        $(reset_overlay_selector).click(function () {
            farmMapUtils.setupOverlay();
        });
    },

    setupOverlay: function setupOverlay() {
        //DRAW POLYGON SETUP
        var drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: ['polygon', 'circle']
            },
            polygonOptions: {
                editable: true
            }

        });
        drawingManager.setMap(current_map);
        allOverlays.push(drawingManager);

        google.maps.event.addListener(drawingManager, 'overlaycomplete', function (event) {
            event.overlay.set('editable', false);
            allOverlays.push(event.overlay);
            farmMapUtils.lastShapePoints = farmMapUtils.getPolygonXY(event.overlay);
            document.getElementById(json_container).innerHTML = (0, _stringify2.default)(farmMapUtils.lastShapePoints);
            drawingManager.setMap(null);
        });
    },

    clearOverlays: function clearOverlays() {
        allOverlays.forEach(function (o) {
            o.setMap(null);
        });
        allOverlays = [];
    },

    //get an array of points labeled with X and Y
    //.getPath() function is the google maps api way of getting coordinates from a shape or object
    getPolygonXY: function getPolygonXY(pathAbleObj) {
        var points = [];
        pathAbleObj.getPath().forEach(function (p) {
            var np = { x: p.lat(), y: p.lng() };
            points.push(np);
        });
        var first = points[0];
        points.push(first);
        return points;
    }
};
//# sourceMappingURL=farm-map-utils.js.map