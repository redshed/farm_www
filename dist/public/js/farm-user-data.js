"use strict";

var _stringify = require("babel-runtime/core-js/json/stringify");

var _stringify2 = _interopRequireDefault(_stringify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var currentList = [];
var currentFarm = null;
var currentMap = null;

farmUserData = {
    //load list of farms for current user
    loadFarms: function loadFarms(load_button_selector, results_table_selector) {
        // Start loading
        $('.spinner').show();

        //add results function
        var add_results_to_table = function add_results_to_table(results) {
            console.log("loadFarmList SUCCESS");

            var mapper = function mapper(val) {
                return {
                    objectId: val.objectId,
                    name: val.name,
                    updatedAt: moment.utc(val.updatedAt).format("MM/DD/YYYY hh:mm A"),
                    propertyCount: val.propertyCount,
                    turnoverRate: val.turnoverRate
                };
            };

            var results_mapped = results.map(mapper);

            var dt = $(results_table_selector).DataTable({
                destroy: true,
                "rowId": "objectId",
                "columns": [{ "data": "objectId", "visible": false }, { "data": "name" }, { "data": "updatedAt" }, { "data": "propertyCount" }, { "data": "turnoverRate" }],
                data: results_mapped
            });

            dt.on('click', 'tbody > tr', function () {
                var data = dt.row(this).data();
                $('#farm_list_display_row').hide();
                $('#farm_detail_display_row').show();
                $('#farm_detail_map').show();
                $('#farm_summary').show();
                $('#page_title_detail').text(data.name);
                farmUserData.loadFarmDetail("#farmDetail", results.filter(function (x) {
                    return x.objectId === data.objectId;
                })[0]);
            });

            currentList = results;
        };

        var url = "/user_data/farm";
        //setup call to data service
        var settings = {
            "async": true,
            "url": url,
            "method": "GET",
            "headers": {
                "content-type": "application/json",
                "cache-control": "no-cache"
            },
            "processData": false
        };

        //display the results
        $.ajax(settings).done(function (response) {
            console.log(response);
            add_results_to_table(response);
        }).fail(function (response) {
            console.log(response);
        }).always(function () {
            $('.spinner').hide();
        });
    },

    //load list of properties for selected farm
    loadFarmDetail: function loadFarmDetail(results_table_selector, farm) {
        var addMarker = function addMarker(map, latlng, icon, title, propId) {
            var marker = new google.maps.Marker({
                position: latlng,
                icon: icon,
                title: title,
                map: map
            });

            google.maps.event.addListener(marker, 'click', function () {
                //show detail edit modal
                $('#' + propId).click();
            });
        };

        //status to icon map
        var icon_base_path = '/img/farmicons/';
        var status_icons = {
            'Not Contacted': icon_base_path + 'marker_not-contacted.png',
            'Not Home': icon_base_path + 'marker_not-home.png',
            'Not Interested': icon_base_path + 'marker_not-interested.png',
            'On Market': icon_base_path + 'marker_on-market.png',
            'Prospect': icon_base_path + 'marker_prospect.png',
            'Lead': icon_base_path + 'marker_lead.png'
        };

        var format_currency = function format_currency(val) {
            return '$' + parseFloat(val).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        };

        //add results function
        var add_results_to_map = function add_results_to_map(farm_to_add) {
            //loop farm and add icon for current status at lat / long
            var bounds = new google.maps.LatLngBounds();

            farm_to_add.properties.forEach(function (prop) {
                //console.log(prop.A041_Latitude);
                //console.log(prop.A042_Longitude);
                var icon = prop.Status ? status_icons[prop.Status] : status_icons['Not Contacted'];
                var latlng = new google.maps.LatLng(parseFloat(prop.A041_Latitude), parseFloat(prop.A042_Longitude));
                addMarker(farmUserData.currentMap, latlng, icon, prop.A017_FullMailAddress, prop.A000_PropertyNumber);
                bounds.extend(latlng);
            });
            farmUserData.currentMap.fitBounds(bounds);
            farmUserData.currentMap.panToBounds(bounds);
        };

        var get_total_value = function get_total_value(prop) {
            var imp_val = parseFloat(prop.A034_AssessedImprovementValue);
            var land_val = parseFloat(prop.A032_AssessedLandValue);
            var total_val = 0.00;
            if (land_val === imp_val) {
                total_val = land_val;
            } else {
                total_val = land_val + imp_val;
            }
            if (isNaN(total_val)) {
                return "";
            } else {
                return format_currency(total_val);
            }
        };

        var add_results_to_table = function add_results_to_table(farm_to_add) {
            console.log("loadFarmDetail SUCCESS");

            var mapper = function mapper(val) {
                return {
                    propId: val.A000_PropertyNumber,
                    Address: val.A017_FullMailAddress,
                    Owner: val.A016_OwnerName + "<br/>" + val.A009_OwnerName2,
                    Status: val.Status,
                    Notes: val.Notes,
                    Email: val.hasOwnProperty("Email") ? val.Email : "",
                    Phone: val.hasOwnProperty("Phone") ? val.Phone : "",
                    OwnerOccupied: val.A040_OwnerOccupied,
                    Value: get_total_value(val),
                    Beds: val.A024_Bedrooms,
                    Baths: val.A025_Bathrooms,
                    SaleDate: val.A030_SaleDate
                };
            };

            var properties_mapped = farm_to_add.properties.map(mapper);

            var dt = $(results_table_selector).DataTable({
                destroy: true,
                "rowId": "propId",
                data: properties_mapped,
                "columns": [{ "data": "propId", "visible": false }, { "data": "Address" }, { "data": "Owner" }, { "data": "Status" }, { "data": "Email" }, { "data": "Phone" }, { "data": "OwnerOccupied" }, { "data": "Value" }, { "data": "Beds" }, { "data": "Baths" }, { "data": "SaleDate" }]
            });

            $(results_table_selector).show();

            //load property detail for editing
            dt.on('click', 'tbody > tr', function () {
                var data = dt.row(this).data();
                var prop = farm_to_add.properties.filter(function (p) {
                    return p.A000_PropertyNumber === data.propId;
                })[0];
                $('#farm_detail_edit').modal();

                //contact info
                $('#farm-detail-propertynumber').val(prop.A000_PropertyNumber);
                $('#farm-detail-status').val(prop.Status);
                $('#farm-detail-address').val(prop.A001_Address);
                $('#farm-detail-apn').val(prop.A037_ParcelNumber);
                $('#farm-detail-ownername').val(prop.A016_OwnerName);
                $('#farm-detail-ownername2').val(prop.A009_OwnerName2);
                $('#farm-detail-mail').val(prop.A017_FullMailAddress);
                $('#farm-detail-phone').val(prop.A044_OwnerPhone);
                $('#farm-detail-email').val(prop.A045_OwnerEmail);
                $('#farm-detail-notes').val(prop.Notes);

                //property detail
                $('#farm-detail-bedrooms').val(prop.A024_Bedrooms);
                $('#farm-detail-bathrooms').val(prop.A025_Bathrooms);
                $('#farm-detail-buildingsqft').val(prop.A022_BuildingArea);
                $('#farm-detail-lotarea').val(prop.A035_LotArea);
                $('#farm-detail-pool').val(prop.A026_Pool);
                $('#farm-detail-yearbuilt').val(prop.A038_YearBuilt);

                //sales detail
                $('#farm-detail-saleprice').val(prop.A029_SalesPrice);
                $('#farm-detail-saledate').val(prop.A030_SaleDate);
                $('#farm-detail-docnumber').val(prop.A031_SalesDocumentNumber);
                $('#farm-detail-assessedland').val(prop.A032_AssessedLandValue);
                $('#farm-detail-assessedbuilding').val(prop.A034_AssessedImprovementValue);
                $('#farm-detail-zoning').val(prop.A021_Zoning);
                $('#farm-detail-usecode').val(prop.A020_UseCodeDescription);

                $('#farm_detail_edit_title').text(prop.A001_Address);
            });

            $('#bt_modal_detail_save').click(function () {
                var propertynumber = $('#farm-detail-propertynumber').val();
                var i = farm_to_add.properties.findIndex(function (p) {
                    return p.A000_PropertyNumber === propertynumber;
                });
                farm_to_add.properties[i].Status = $('#farm-detail-status').val();
                farm_to_add.properties[i].A001_Address = $('#farm-detail-address').val();
                farm_to_add.properties[i].A037_ParcelNumber = $('#farm-detail-apn').val();
                farm_to_add.properties[i].A016_OwnerName = $('#farm-detail-ownername').val();
                farm_to_add.properties[i].A009_OwnerName2 = $('#farm-detail-ownername2').val();
                farm_to_add.properties[i].A017_FullMailAddress = $('#farm-detail-mail').val();
                farm_to_add.properties[i].A044_OwnerPhone = $('#farm-detail-phone').val();
                farm_to_add.properties[i].A045_OwnerEmail = $('#farm-detail-email').val();
                farm_to_add.properties[i].Notes = $('#farm-detail-notes').val();

                //property detail
                farm_to_add.properties[i].A024_Bedrooms = $('#farm-detail-bedrooms').val();
                farm_to_add.properties[i].A025_Bathrooms = $('#farm-detail-bathrooms').val();
                farm_to_add.properties[i].A022_BuildingArea = $('#farm-detail-buildingsqft').val();
                farm_to_add.properties[i].A035_LotArea = $('#farm-detail-lotarea').val();
                farm_to_add.properties[i].A026_Pool = $('#farm-detail-pool').val();
                farm_to_add.properties[i].A038_YearBuilt = $('#farm-detail-yearbuilt').val();

                //sales detail
                farm_to_add.properties[i].A029_SalesPrice = $('#farm-detail-saleprice').val();
                farm_to_add.properties[i].A030_SaleDate = $('#farm-detail-saledate').val();
                farm_to_add.properties[i].A031_SalesDocumentNumber = $('#farm-detail-docnumber').val();
                farm_to_add.properties[i].A032_AssessedLandValue = $('#farm-detail-assessedland').val();
                farm_to_add.properties[i].A034_AssessedImprovementValue = $('#farm-detail-assessedbuilding').val();
                farm_to_add.properties[i].A021_Zoning = $('#farm-detail-zoning').val();
                farm_to_add.properties[i].A020_UseCodeDescription = $('#farm-detail-usecode').val();

                //replace property at propertynumber and save farm
                var url = "/user_data/farm/" + farm_to_add.objectId;
                var data = { "properties": farm_to_add.properties };
                var settings = {
                    "async": false,
                    "url": url,
                    "method": "PUT",
                    "headers": {
                        "content-type": "application/json",
                        "cache-control": "no-cache"
                    },
                    data: (0, _stringify2.default)(data)
                };

                //display the results
                $.ajax(settings).done(function (response) {
                    console.log(response);
                }).fail(function (response) {
                    console.log(response);
                }).always(function () {
                    $('#farm_detail_edit').modal('hide');
                });
            });
        };

        var add_results_to_snapshot = function add_results_to_snapshot(farm) {
            //12 MO HISTORY
            //# of sales
            //let sales = farm.filter(prop =>  prop.A030_SaleDate > )
            //total properties
            var prop_count = farm.properties.length;
            var one_yr_props = [];
            //turnover %
            var startDate = dateFns.subYears(Date.now(), 1);
            var yearSales = 0;

            //TODO: this could just be a map
            farm.properties.forEach(function (x) {
                var sale_date = x.A030_SaleDate !== null ? dateFns.parse(x.A030_SaleDate) : null;
                if (sale_date !== null && dateFns.isAfter(sale_date, startDate)) {
                    one_yr_props.push(x);
                }
            });
            //if nothing in the last year / skip
            if (one_yr_props.length > 0) {
                var turnover_percent = (one_yr_props.length / prop_count * 100).toFixed(1) + "%";

                //median sale price
                var median_sale_price = math.median(_.pluck(one_yr_props, 'A029_SalesPrice'));
                ;
                //max sale price
                var max_sale_price = math.max(_.pluck(one_yr_props, 'A029_SalesPrice'));

                //min sale price
                var min_sale_price = math.min(_.pluck(one_yr_props, 'A029_SalesPrice'));

                $('#snapshot_median_sales').text(format_currency(median_sale_price));
                $('#snapshot_max_sale').text(format_currency(max_sale_price));
                $('#snapshot_min_sale').text(format_currency(min_sale_price));
                $('#snapshot_number_of_sales').text(one_yr_props.length);
                $('#snapshot_turnover').text(turnover_percent);
            } else {
                $('#yr_snapshot').hide();
            }
            //SNAPSHOT
            //median beds
            var median_bed = math.median(_.pluck(farm.properties, 'A024_Bedrooms'));
            //median baths
            var median_bath = math.median(_.pluck(farm.properties, 'A025_Bathrooms'));
            //owner occupied count
            var owner_occ = farm.properties.filter(function (prop) {
                return prop.A040_OwnerOccupied === "Y";
            });
            $('#snapshot_median_beds').text(median_bed);
            $('#snapshot_median_baths').text(median_bath);
            $('#snapshot_num_owner_occ').text(owner_occ.length);
        };

        add_results_to_table(farm);
        add_results_to_map(farm);
        add_results_to_snapshot(farm);
        //TODO: save detail from modal save click event
    }
};
//# sourceMappingURL=farm-user-data.js.map