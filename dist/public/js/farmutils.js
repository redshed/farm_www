'use strict';

farmMapUtils = {
    lastShape: null,

    //bind the map
    //set up address autocomplete
    //set up polygon drawing
    //called by googleapis callback
    setupMap: function setupMap(map_id, text_autocomplete_id, json_debug_text_id) {
        //MAP SETUP
        var mapOptions = {
            zoom: 16,
            //center: centerMapOn,
            mapTypeId: google.maps.MapTypeId.RoadMap
        };
        map = new google.maps.Map(document.getElementById(map_id), mapOptions);

        //ADDRESS AUTOCOMPLETE
        var input = document.getElementById(text_autocomplete_id);
        var options = {
            types: ['address']
        };

        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });

        var autocomplete = new google.maps.places.Autocomplete(input, options);

        autocomplete.addListener('place_changed', function () {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                //map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            //DRAW POLYGON SETUP
            var drawingManager = new google.maps.drawing.DrawingManager({
                drawingMode: google.maps.drawing.OverlayType.POLYGON,
                drawingControl: true,
                drawingControlOptions: {
                    position: google.maps.ControlPosition.TOP_CENTER,
                    drawingModes: ['polygon', 'circle']
                },
                polygonOptions: {
                    editable: true
                }

            });
            drawingManager.setMap(map);

            google.maps.event.addListener(drawingManager, 'overlaycomplete', function (event) {
                event.overlay.set('editable', false);
                farmMapUtils.lastShape = farmMapUtils.getPolygonXY(event.overlay);
                if (json_debug_text_id) {
                    document.getElementById(json_debug_text_id).innerHTML = JSON.stringify(farmMapUtils.lastShape);
                }
                drawingManager.setMap(null);
            });
        });
    },

    //get an array of points labeled with X and Y
    //.getPath() function is the google maps api way of getting coordinates from a shape or object
    getPolygonXY: function getPolygonXY(pathAbleObj) {
        var points = [];
        pathAbleObj.getPath().forEach(function (p) {
            var np = { x: p.lat(), y: p.lng() };
            points.push(np);
        });
        var first = points[0];
        points.push(first);
        return points;
    }
};
//# sourceMappingURL=farm-map-utils.js.map