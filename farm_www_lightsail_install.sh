#!/bin/bash
sudo apt-get update -y
sudo apt-get install nodejs -y
sudo apt-get install npm -y
echo "fix node name on ubuntu"
sudo ln -s /usr/bin/nodejs /usr/bin/node
echo "update node version"
sudo npm install -g n
sudo n 8.5.0
sudo mkdir /opt/apps
cd /opt/apps
echo "clone repo"
sudo git clone https://git-codecommit.us-east-1.amazonaws.com/v1/repos/farm_www
cd farm_www
echo "install dependencies"
sudo npm install
export NODE_ENV="production"
read -p "set NODE_ENV to production: NODE_ENV=\"production\""
sudo vi /etc/environment
#change hostname
sudo hostnamectl set-hostname farm_www
read -p "add hostname 127.0.0.1 farm_www"
sudo vi /etc/hosts

echo "install node process manager"
sudo npm install -g pm2 -y
cd /opt/apps/farm_www
sudo pm2 start ./bin/www --name "farm_www"
sudo pm2 save
echo "run the script"
sudo pm2 startup
echo "reboot to test - sudo reboot"