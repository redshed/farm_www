// const authorized = function(req, res, next)  {
//     if(req.user) {
//         return next();
//     }
//     return res.redirect('/login');
// };

const Hashids = require('hashids');
const hashids = new Hashids('Red Shed');

const hash_encode = function( s ) {
    let hex = new Buffer(s).toString('hex');
    let encoded = hashids.encodeHex(hex);
    return encoded;
};

const hash_decode = function( h ) {
    let decodedHex = hashids.decodeHex(h);
    let s = new Buffer(h, 'hex').toString('utf8');
    return s;
};

const ua = require('universal-analytics');
const init_ua_visitor = function(ga_id, user_id) {
    let visitor = ua(ga_id, user_id, {strictCidFormat: false});
    return visitor;
};


module.exports = {hash_encode, hash_decode, init_ua_visitor};