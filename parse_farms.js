const logger = require('./detail_logger').logger;

const Parse = require('parse/node');
const config = require('config');

const parse_server_url = config.get("parse_server_url");
const parse_server_token = config.get("parse_server_token");

class ParseFarms {
    constructor() {
    }

    async loadFarms(app_name, email) {
        logger.verbose("calling parse server to list farms");
        Parse.initialize(parse_server_token);
        Parse.serverURL = parse_server_url;
        let Farm  = Parse.Object.extend("Farm");
        let query = new Parse.Query(Farm);
        query.equalTo("email", email);
        query.equalTo("isDeleted", false);
        let farms = await query.find();
        logger.verbose("list farms completed");
        return farms.map(f => f.toJSON());
    }

    async saveFarm(app_name, new_farm) {
        logger.verbose("calling parse server to save farm");
        Parse.initialize(parse_server_token);
        Parse.serverURL = parse_server_url;
        let Farm = Parse.Object.extend("Farm");
        let parse_farm = new Farm();
        parse_farm.save(new_farm, {
            success: function (farm) {
                // Execute any logic that should take place after the object is saved.
                logger.verbose('New farm created with objectId: ' + farm.id);
                return new_farm;
            },
            error: function (farm, error) {
                // Execute any logic that should take place if the save fails.
                // error is a Parse.Error with an error code and message.
                logger.error('Failed to create new farm, with error code: ' + error.message);
                return {error: error};
            }
        });
    }

    async updateFarmProperties(objectId, properties) {
        logger.verbose("calling parse server to update farm");
        Parse.initialize(parse_server_token);
        Parse.serverURL = parse_server_url;
        let Farm = Parse.Object.extend("Farm");
        let query = new Parse.Query(Farm);
        query.get(objectId, {
            success: function(farm) {
                farm.set("properties", properties);
                farm.save(null, {
                    success: function (o) {
                        console.log(o);
                        return o;
                    },
                    error: function (model, error) {
                        console.log("parse_farms updateFarmProperties ERROR " + error);
                        return {error: error};
                    }
                });
            },
            error: function(object, error) {
               logger.error("failed to get farm for objectId " + objectId + " - " + error);
            }

        });
    }
}

const FarmJson = {
    "objectId": "",
    "filters": "{\"MaxBedrooms\":null,\"MaxBathrooms\":null,\"SaleTypes\":null,\"MinBedrooms\":null,\"UseCodes\":null,\"EndDate\":null,\"MinBathrooms\":null,\"OwnerOccupied\":null,\"StartDate\":null}",
    "shapeCoordinates": null,
    "properties": null,
    "reportDate": null,
    "reportCaveats": [],
    "email": "",
    "reportVersion": 1,
    "isDeleted": false,
    "appName": "",
    "indexOfLastPropertyViewed": 0,
    "turnoverRate": "",
    "nameLower": "",
    "propertyCount": 0,
    "webServiceId": "",
    "name": "",
    "isPurchased": false,
    "createdAt": null,
    "updatedAt": null
};

module.exports = {ParseFarms: ParseFarms, FarmJson: FarmJson};