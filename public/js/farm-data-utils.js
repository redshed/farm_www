//utilities for PFData
farmDataUtils = {
    loadDataPFData: function (run_farm_button_selector, results_table_selector, shape_json, provider, export_to_csv = false) {
        // Start loading
        $(run_farm_button_selector).prop('value', 'Running').prop('disabled', true);

        //add results function
        let search_complete = function (results, shape_json) {
            console.log("loadDataPFData SUCCESS");

            //default / datatree
            let mapper = (val) => {
                return [val.A037_ParcelNumber, val.A002_HouseNumber + ' ' + val.A004_StreetName + ' ' + val.A006_CityState, val.A016_OwnerName,
                    val.A024_Bedrooms, val.A025_Bathrooms, val.A032_AssessedLandValue, val.A034_AssessedImprovementValue, val.A040_OwnerOccupied];
            };

            // if (provider === "bk") {
            //     mapper = (val) => {
            //         return [val.A037_ParcelNumber, val.A002_HouseNumber + ' ' + val.A004_StreetName + ' ' + val.A006_CityState, val.A009_OwnerName2];
            //     }
            // };

            if (results) {
                let results_mapped = results.Properties.map(mapper);

                $(results_table_selector).DataTable({
                    destroy: true,
                    data: results_mapped
                });

                if (export_to_csv) {
                    farmDataUtils.JSONToCSVConvertor(results.Properties, provider);
                }
            }

            $('#bt_save_farm')
                .click(function () {
                    //post data to /user_data/
                    //inject notes and status into response
                    let new_farm = results;
                    new_farm.Properties = results.Properties.map( function(e) {
                            e.Notes = "";
                            e.Status = "Not Contacted";
                            return e;
                        });
                    new_farm.name = $('#farm_name').val();
                    new_farm.shapeCoordinates = shape_json.Shape;
                    $.post("/user_data/farm",
                        results,
                        function(data, textStatus) {
                            console.log(data);
                            console.log(textStatus);
                        },
                        'json');
                });
        };

        //show error function
        let show_error = function (response) {
            console.log("ERROR");
            console.log(r);
        };

        let url = "/pfdata/farm?provider=" + provider;
        //setup call to data service
        let request_data = {
            Shape: null,
            Filters: {
                FromSaleDate:"",
                ToSaleDate:"",
                UseCode:"",
                BedroomsMin:"",
                BedroomsMax:"",
                BathroomsMin:"",
                BathroomsMax:"",
                OwnerOccupied:""},
            Options: {TaxData: false}
        };
        request_data.Shape = shape_json.Shape;
        //get filters

        if ($('#farm_filter_bedrooms_min').val()) {
            request_data.Filters.BedroomsMin = $('#farm_filter_bedrooms_min').val();
        };
        if ($('#farm_filter_bedrooms_max').val()) {
            request_data.Filters.BedroomsMax = $('#farm_filter_bedrooms_max').val();
        };
        if ($('#farm_filter_baths_min').val()) {
            request_data.Filters.BathroomsMin = $('#farm_filter_baths_min').val();
        };
        if ($('#farm_filter_baths_max').val()) {
            request_data.Filters.BathroomsMax = $('#farm_filter_baths_max').val();
        };
        if ($('#farm_filter_owner_occupied').val() && $('#farm_filter_owner_occupied').bootstrapSwitch('state')) {
            request_data.Filters.OwnerOccupied = true;
        };
        if ($('#farm_filter_from_sale_date').val()) {
            request_data.Filters.FromSaleDate = $('#farm_filter_from_sale_date').val();
        };
        if ($('#farm_filter_to_sale_date').val()) {
            request_data.Filters.ToSaleDate = $('#farm_filter_to_sale_date').val();
        };
        if ($('#farm_filter_use_codes').val()) {
            request_data.Filters.UseCode = $('#farm_filter_use_codes').val();
        };
        console.log(JSON.stringify(request_data));
        let settings = {
            "async": true,
            "crossDomain": true,
            "url": url,
            "method": "POST",
            "headers": {
                "content-type": "application/json",
                "cache-control": "no-cache"
            },
            "processData": false,
            "data": JSON.stringify(request_data)
        };

        //display the results
        $.ajax(settings)
            .done(function (response) {
                console.log(response);
                search_complete(response, shape_json);
            })
            .fail(function (response) {
                console.log(response)
            })
            .always(function (response) {
                $(run_farm_button_selector).prop('value', 'Run Farm').prop('disabled', false);
            });
    },

    JSONToCSVConvertor: function (JSONData, provider) {
        let field_names = ['A000_PropertyNumber','A001_Address','A002_HouseNumber','A003_StreetPrefix','A004_StreetName','A005_UnitNumber','A006_CityState','A007_ZipCode','A008_Zip4','A009_OwnerName2','A010_Owner1FirstName','A011_Owner1MiddleName','A012_Owner1LastName','A013_Owner2FirstName','A014_Owner2MiddleName','A015_Owner2LastName','A016_OwnerName','A017_FullMailAddress','A018_MailAddressCityState','A019_MailAddressZip4','A020_UseCodeDescription','A021_Zoning','A022_BuildingArea','A023_Rooms','A024_Bedrooms','A025_Bathrooms','A026_Pool','A027_NumberofStories','A028_NumberofUnits','A029_SalesPrice','A030_SaleDate','A031_SalesDocumentNumber','A032_AssessedLandValue','A033_AssessedImprovePercent','A034_AssessedImprovementValue','A035_LotArea','A036_LotAreaUnits','A037_ParcelNumber','A038_YearBuilt','A039_TBMapPageGrid','A040_OwnerOccupied','A041_Latitude','A042_Longitude','A043_Homeowner_Exemption','A044_OwnerPhone','A045_OwnerEmail','A046_City','A047_State','Notes','Status'];
        //let dt_field_names = ['situs_addressLine','situs_addressMatchResults','situs_addressTypeCode','situs_addressTypeString','situs_carrierRoute','situs_cbsaCode','situs_cbsaDivisionCode','situs_cbsaDivisionLevel','situs_cbsaDivisionTitle','situs_cbsaLevel','situs_cbsaTitle','situs_censusBlock','situs_censusTract','situs_city','situs_cityAbbreviation','situs_congressionalDistrict','situs_countryCode','situs_countyFips','situs_countyName','situs_deliveryPointCheckDigit','situs_deliveryPointCode','situs_dpvFootnotes','situs_eLotNumber','situs_eLotOrder','situs_ewsFlag','situs_geoErrorCode','situs_geoStatusCode','situs_lacs','situs_lacsLinkIndicator','situs_lacsLinkReturnCode','situs_latitude','situs_longitude','situs_msa','situs_placeCode','situs_placeName','situs_plus4','situs_pmsa','situs_postDir','situs_preDir','situs_privateMailbox','situs_rbdi','situs_recordId','situs_state','situs_streetName','situs_streetNumber','situs_streetType','situs_suite','situs_suiteLinkReturnCode','situs_suiteType','situs_timeZone','situs_timeZoneCode','situs_urbanization','situs_zip','situs_zipType','AirConditioning','Apn','Bathrooms','Bedrooms','FipsCode','ForeclosureIndicator','ForeclosureIndicatorType','LandUseCategory','LastMarketSaleDate','LastMarketSalePrice','Latitude','Longitude','LotSqrft','mailing_addressLine','mailing_addressMatchResults','mailing_addressTypeCode','mailing_addressTypeString','mailing_carrierRoute','mailing_cbsaCode','mailing_cbsaDivisionCode','mailing_cbsaDivisionLevel','mailing_cbsaDivisionTitle','mailing_cbsaLevel','mailing_cbsaTitle','mailing_censusBlock','mailing_censusTract','mailing_city','mailing_cityAbbreviation','mailing_congressionalDistrict','mailing_countryCode','mailing_countyFips','mailing_countyName','mailing_deliveryPointCheckDigit','mailing_deliveryPointCode','mailing_dpvFootnotes','mailing_eLotNumber','mailing_eLotOrder','mailing_ewsFlag','mailing_geoErrorCode','mailing_geoStatusCode','mailing_lacs','mailing_lacsLinkIndicator','mailing_lacsLinkReturnCode','mailing_latitude','mailing_longitude','mailing_msa','mailing_placeCode','mailing_placeName','mailing_plus4','mailing_pmsa','mailing_postDir','mailing_preDir','mailing_privateMailbox','mailing_rbdi','mailing_recordId','mailing_state','mailing_streetName','mailing_streetNumber','mailing_streetType','mailing_suite','mailing_suiteLinkReturnCode','mailing_suiteType','mailing_timeZone','mailing_timeZoneCode','mailing_urbanization','mailing_zip','mailing_zipType','NumberOfBusinessCommercialUnits','NumberOfResidentialUnits','NumberOfStories','OwnerFirstName_MI1','OwnerFirstName_MI2','OwnerFullName1','OwnerFullName2','OwnerFullName3','OwnerFullName4','OwnerLastname1','OwnerLastname2','OwnerName1','OwnerSuffix1','OwnerSuffix2','Parcel','ParsedNames','PoolCode','PrivacyFlag','PropertyId','ResidenceSqrft','Status','TotalAssessedValue','UseCode','YearBuilt'];

        //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
        let arrData = typeof JSONData !== 'object' ? JSON.parse(JSONData) : JSONData;

        let CSV = '';
        //Set Report title in first row or line

        //add the field names to the first row
        arrData.unshift(field_names);

        //1st loop is to extract each row
        for (let i = 0; i < arrData.length; i++) {
            let row = "";
            let flat_row = farmDataUtils.flattenObject(arrData[i]);
            row = flat_row.join(",");
            //add a line break after each row
            CSV += row + '\r\n';
        }

        if (CSV === '') {
            alert("Invalid data");
            return;
        }

        //Generate a file name
        let fileName = "Farm_Results";

        //Initialize file format you want csv or xls
        let uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

        // Now the little tricky part.
        // you can use either>> window.open(uri);
        // but this will not work in some browsers
        // or you will not get the correct file extension

        //this trick will generate a temp <a /> tag
        let link = document.createElement("a");
        link.href = uri;

        //set the visibility hidden so it will not effect on your web-layout
        link.style = "visibility:hidden";
        link.download = fileName + ".csv";

        //this part will append the anchor tag and remove it after automatic click
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    },

    //returns array
    flattenObject: function (ob) {
        let toReturn = {};

        for (let i in ob) {
            if (!ob.hasOwnProperty(i)) continue;

            if ((typeof ob[i]) === 'object' && ob[i] !== null) {
                let flatObject = farmDataUtils.flattenObject(ob[i]);
                for (let x in flatObject) {
                    if (!flatObject.hasOwnProperty(x)) continue;
                    toReturn[i + '.' + x] = flatObject[x];
                }
            } else {
                toReturn[i] = ob[i];
            }
        }
        return Object.values(toReturn);


    }
};
