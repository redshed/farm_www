const request = require("request-promise");
const _ = require("underscore");
const btoa = require("btoa");
const logger = require('./detail_logger').logger;

class RedShedTitleService {
    constructor () {
        this.default_options = {
            method: 'GET',
            url: '',
            qs:
                {
                    username: '',
                    office: '',
                    password: '',
                    devicedata: ''
                },
            headers:
                {
                    'cache-control': 'no-cache'
                }
        }
    }

    async authenticateUser(username, office, password) {
        let device_data = `APPNAME|PropertyForce,APPVERSION|4.0.0.0-DEV,USERNAME|${username},OFFICECODE|${office},DEVICETYPE|samsung SM-G920V,DEVICEDIMENSIONS|Undetermined,OS|Android,OSVERSION|5.0.2`;
        let options = _.clone(this.default_options);
        let status = "FAIL";
        let message = "";
        let key = "";
        options.qs.username = username;
        options.qs.office = office;
        options.qs.password = btoa(password);
        options.qs.devicedata = device_data;
        options.url = 'https://test.propertyforcemobile.com/v4test2/TitleService.svc/Secure/AuthenticateUser';
        let user_response = await request(options);
        user_response = JSON.parse(user_response);

        status = user_response.d.StatusCode;
        message = user_response.d.Status;

        if (status === "OK") {
            options.url = 'https://test.propertyforcemobile.com/v4test2/TitleService.svc/Secure/GetRepDataWithKey';
            let key_response = await request(options);
            key_response = JSON.parse(key_response);
            try {
                if (key_response.d.Key && key_response.d.Key.length > 0) {
                    let full_key = JSON.parse(key_response).d.Key;
                    key = full_key.split("_")[0];
                }
                else {
                    status = key_response.d.Status;
                    message = key_response.d.StatusCode;
                }
            }
            catch (e) {
                logger.error("Error in titleservice authenticateUser");
            }
        }
        return {username: username, key: key, status: status, message: message};
    }
}

module.exports = {RedShedTitleService: RedShedTitleService};