/**
 * Created by dvicente@solidgear.es on 10/11/2017
 */
'use strict'

const winston = require('winston');
require('winston-papertrail').Papertrail;
const uuid = require('uuid');
const contextService = require('request-context');
const config = require('config');
const environments = require("./config/environments.js").environments; //not sure there is a better way to basically get an enum
const env = config.get("label");

//LOGGING
//set console for local development
//papertrail for the rest
let winston_transports =[
    new winston.transports.Papertrail({
        host: 'logs4.papertrailapp.com',
        port: 25947,
        program: 'farm_www',
        colorize: true,
        level: 'info'
    })];

if (env === environments.development || env === environments.autotest) {
    winston_transports = [new winston.transports.Console({
        colorize: true,
        level: 'debug'
    })];
}

const winstonLogger = new winston.Logger({
    transports: winston_transports,
    exitOnError: false
});

winston.configure({
    transports: winston_transports,
    exceptionHandlers: winston_transports
});

winstonLogger.stream = {
    write: function (message, encoding) {
        winstonLogger.info(message);
    }
};

// Wrap Winston logger to print reqId in each log
const formatMessage = function(message) {
    let request_token = getToken();
    message = request_token ? request_token + " " + message : message;
    return message;
};

const logger = {
    log: function(level, message) {
        winstonLogger.log(level, formatMessage(message));
    },
    error: function(message) {
        winstonLogger.error(formatMessage(message));
    },
    warn: function(message) {
        winstonLogger.warn(formatMessage(message));
    },
    verbose: function(message) {
        winstonLogger.verbose(formatMessage(message));
    },
    info: function(message) {
        winstonLogger.info(formatMessage(message));
    },
    debug: function(message) {
        winstonLogger.debug(formatMessage(message));
    },
    silly: function(message) {
        winstonLogger.silly(formatMessage(message));
    }
};

const setToken = (tag) => {
    contextService.set("request:token", tag + "-" + uuid.v4().replace(/-/g, "").substring(0,16));
};

const getToken = () => {
    return contextService.get("request:token");
};


module.exports = {logger, winston_transports, setToken, getToken};