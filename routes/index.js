// routes/index.js
const express = require('express');
const passport = require('passport');
const router = express.Router();
const config = require('config');
const logger = require('../detail_logger.js').logger;

const env = {
    AUTH0_CLIENT_ID: config.get("auth0_client_id"),
    AUTH0_DOMAIN: config.get("auth0_domain"),
    AUTH0_CALLBACK_URL: config.get("auth0_callback_url")
};

/* GET home page. */
router.get('/', function(req, res, next) {
    res.redirect('/main/farm');
});

// Perform the login
router.get(
    '/login',
    passport.authenticate('auth0', {
        clientID: env.AUTH0_CLIENT_ID,
        domain: env.AUTH0_DOMAIN,
        redirectUri: env.AUTH0_CALLBACK_URL,
        audience: 'https://' + env.AUTH0_DOMAIN + '/userinfo',
        responseType: 'code',
        scope: 'openid profile'
    }),
    function(req, res) {
        res.redirect('/');
    }
);

// Perform session logout and redirect to homepage
router.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/main/farm');
});

// Perform the final stage of authentication and redirect to '/user'
router.get(
    '/callback',
    passport.authenticate('auth0', {
        failureRedirect: '/'
    }),
    function(req, res) {
        res.redirect(req.session.returnTo || '/main/farm');
    }
);

module.exports = router;