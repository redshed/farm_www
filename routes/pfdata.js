const express = require('express');
const router = express.Router();
const config = require('config');
const request = require('superagent');
const querystring = require('querystring');

//const authorized = require('../utility').authorized;
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const detailLogger = require('../detail_logger.js');
const logger = detailLogger.logger;


//farm search
router.post('/farm', ensureLoggedIn, function(req, res, next) {
    //better to do this as a pass-through to hide auth
    res.setHeader('Content-Type', 'application/json');
    let token = config.get("pfdata_access_token");
    let url = config.get("pfdata_url") +  "/api/farm?access_token=" + token + "&" + querystring.stringify(req.query);
    logger.verbose("request to: " + url.replace(token, "TOKEN_MASK"));
    request.post(url)
        .send(req.body)
        .set('accept', 'json')
        .end((err, api_res) => {
            // Calling the end function will send the request
            logger.debug("response:" + JSON.stringify(api_res.body));
            res.end(JSON.stringify(api_res.body));
        });

});

module.exports = router;