const express = require('express');
const router = express.Router();
const config = require('config');
//const authorized = require('../utility').authorized;
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const logger = require('../detail_logger.js').logger;
const ParseFarms = require('../parse_farms');

//get / list farms
router.get('/farm', ensureLoggedIn, function(req, res, next) {
    logger.verbose("calling parse server to list farms");
    res.setHeader('Content-Type', 'application/json');
    let app_name = config.get("parse_server_appname");
    let parse_farms = new ParseFarms.ParseFarms();
    let email = req.user.email || req.session.passport.user.displayName;
    parse_farms.loadFarms(app_name, email)
        .then(function (r) {
            //return json list of farms
            if (r) {
                logger.verbose("/user-data/farm found " + r.length + " farms")
                res.end(JSON.stringify(r));
            }
            else {
                res.end("");
            }
        })
        .catch(function (e) {
            logger.error(e);
            res.end("");
        });
});

router.put('/farm/:objectId', ensureLoggedIn, function(req, res, next) {
    //save to parse server
    logger.verbose("calling parse server to update farm");
    res.setHeader('Content-Type', 'application/json');

    let objectId = req.params.objectId;
    let properties = req.body.properties;
    let parse_farms = new ParseFarms.ParseFarms();

    //TODO: for now only update the properties array, maybe other edits later (like name)
    parse_farms.updateFarmProperties(objectId, properties)
        .then(function (r) {
            //return json list of farms
            if (r) {
                logger.verbose("/user-data/farm updated " + objectId);
                res.end(JSON.stringify(r));
            }
            else {
                res.end("");
            }
        })
        .catch(function (e) {
            logger.error(e);
            res.end("");
        });
});

//save new farm
router.post('/farm', ensureLoggedIn, function(req, res, next) {
    //save to parse server
    logger.verbose("calling parse server to save farm");
    res.setHeader('Content-Type', 'application/json');
    let app_name = config.get("parse_server_appname");
    let parse_farms = new ParseFarms.ParseFarms();
    let new_farm = ParseFarms.FarmJson;
    let email = req.user.email || req.session.passport.user.displayName;
    new_farm.appName = app_name;
    new_farm.properties = req.body.Properties;
    new_farm.propertyCount = parseInt(req.body.Count) || 0;
    new_farm.turnoverRate = req.body.TurnoverPercent;
    new_farm.shapeCoordinates = JSON.stringify(req.body.shapeCoordinates);
    new_farm.reportDate = new Date();
    new_farm.webServiceId = req.body.Token;
    new_farm.email = email;
    new_farm.name = req.body.name;
    new_farm.nameLower = new_farm.name.toLowerCase();

    parse_farms.saveFarm(app_name, new_farm)
        .then(function (r) {
            //return json list of farms
            if (r) {
                logger.verbose("/user-data/farm saved " + new_farm.objectId);
                res.end(JSON.stringify(r));
            }
            else {
                res.end("");
            }
        })
        .catch(function (e) {
            logger.error(e);
            res.end("");
        });
});


module.exports = router;